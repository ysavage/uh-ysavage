/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import org.junit.Test;

/**
 *
 * @author yusufu.savage
 */
public class DisplaySelectionDialogTest {
    
    public DisplaySelectionDialogTest() {
    }
    /**
     * Test of readFileDisplay method, of class DisplaySelectionDialog.
     */
    @Test
    public void testReadFileDisplay() {
        DisplaySelectionDialog instance = new DisplaySelectionDialog();
        instance.readFileDisplay();
    }

    /**
     * Test of countLineDisplay method, of class DisplaySelectionDialog.
     */
    @Test
    public void testCountLineDisplay() throws Exception {
        DisplaySelectionDialog instance = new DisplaySelectionDialog();
        instance.countLineDisplay();
    }

    /**
     * Test of addDataDisplay method, of class DisplaySelectionDialog.
     */
    @Test
    public void testAddDataDisplay() throws Exception {
        String date = "15-Apr-20";
        String sTime = "09:00";
        String eTime = "10:00";
        String tutor = "Program Design";
        String subject = "Prof. Cheng";
        String stdName = "yusufu";
        DisplaySelectionDialog instance = new DisplaySelectionDialog();
        instance.countLineDisplay();
        instance.addDataDisplay(date, sTime, eTime, tutor, subject, stdName);
    }
    
}
