/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yusufu.savage
 */
public class StudentLoginTest {
    
    public StudentLoginTest() {
    }
    /**
     * Test of readFile method, of class StudentLogin.
     */
    @Test
    public void testReadFile() {
        StudentLogin instance = new StudentLogin();
        instance.readFile();
    }

    /**
     * Test of addData method, of class StudentLogin.
     */
    @Test
    public void testAddData() throws Exception {
        String std = "yusufu";
        String prt = "ms. thomas";
        StudentLogin instance = new StudentLogin();
        instance.addData(std, prt);
    }

    /**
     * Test of logicStudent method, of class StudentLogin.
     */
    @Test
    public void testLogicStudent() throws Exception {
        String student = "mariam";
        StudentLogin instance = new StudentLogin();
        instance.logicStudent(student);
    }

    /**
     * Test of logicParent method, of class StudentLogin.
     */
    @Test
    public void testLogicParent() throws Exception {
        String parent = "mr. waller";
        StudentLogin instance = new StudentLogin();
        instance.logicParent(parent);
    }

    /**
     * Test of countLine method, of class StudentLogin.
     */
    @Test
    public void testCountLine() throws Exception {
        StudentLogin instance = new StudentLogin();
        instance.countLine();
    }

    /**
     * Test of studentName method, of class StudentLogin.
     */
    @Test
    public void testStudentName() {
        String expResult = null;
        String result = StudentLogin.studentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of parentName method, of class StudentLogin.
     */
    @Test
    public void testParentName() {
        String expResult = null;
        String result = StudentLogin.parentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of linkBotton method, of class StudentLogin.
     */
    @Test
    public void testLinkBotton() {
        String expResult = null;
        String result = StudentLogin.linkBotton();
        assertEquals(expResult, result);
    }
    
}
