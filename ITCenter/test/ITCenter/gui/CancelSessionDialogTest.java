/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yusufu.savage
 */
public class CancelSessionDialogTest {
    
    public CancelSessionDialogTest() {
    }
    
    /**
     * Test of countLineCancel method, of class CancelSessionDialog.
     */
    @Test
    public void testCountLineCancel() throws Exception {
        CancelSessionDialog instance = new CancelSessionDialog();
        instance.countLineCancel();
    }

    /**
     * Test of writeToFile method, of class CancelSessionDialog.
     */
    
    @Test
    public void testWriteToFile() {
        ArrayList<String> list = CancelSessionDialog.storage("C:\\\\ITCenter\\\\lessons.txt");
        CancelSessionDialog instance = new CancelSessionDialog();
        instance.writeToFile(list);
    }

    /**
     * Test of deletFromFile method, of class CancelSessionDialog.
     */
   
    @Test
    public void testDeletFromFile() {
        
        ArrayList<String> list = CancelSessionDialog.storage("C:\\\\ITCenter\\\\lessons.txt");
        int search = 3;
        CancelSessionDialog instance = new CancelSessionDialog();
        instance.deletFromFile(list, search);
    }

    /**
     * Test of storage method, of class CancelSessionDialog.
     */
    @Test
    public void testStorage() {
        String name_of_file = "";
        ArrayList<String> expResult = null;
        ArrayList<String> result = CancelSessionDialog.storage(name_of_file);
        assertEquals(expResult, result);
    }
    
}
