/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
/**
 *
 * @author yusufu.savage
 */
public class EnvSettings {
   int ln;
    
    public void setEnvironment() throws IOException{
        
        // Create structured directory within C:\
        File directory = new File("C:/ITCenter");
        boolean successful = directory.mkdir();
        // If directory is successfully created
        if (successful){
            // Create the following files required by the application
            FileWriter f= new FileWriter ("C:/ITCenter/timetable.txt");
            FileWriter g= new FileWriter ("C:/ITCenter/students.txt");
            FileWriter i= new FileWriter ("C:/ITCenter/lessons.txt");
            FileWriter j= new FileWriter ("C:/ITCenter/meeting.txt");
            FileWriter k= new FileWriter ("C:/ITCenter/parents.txt");
            FileWriter l= new FileWriter ("C:/ITCenter/schedule.txt");
            // Success message
//            System.out.println("Directory and required files created successfully");
        }else{
            // Operation unsuccessful message
//            System.out.println("Operation failed: Directory must have been created");
        }
        RandomAccessFile rafStr = new RandomAccessFile("C:/ITCenter/students.txt", "rw");
        for (int i=0;i<ln;i++){
            rafStr.readLine();
        }
        rafStr.writeBytes("Student:Assatu"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Yusufu"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Hashim"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Fatumata"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Thomas"+ "\r\n");
        rafStr.writeBytes("Parentname:Mr. Waller"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Victor"+ "\r\n");
        rafStr.writeBytes("Parentname:Mr. Jackson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Alice"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Maritta"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Angeline"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Arthur"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Johnson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Anthony"+ "\r\n");
        rafStr.writeBytes("Parentname:Mr. Jackson"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Lisa"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Thomas"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Steven"+ "\r\n");
        rafStr.writeBytes("Parentname:Mr. Daniels"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Stepheny"+ "\r\n");
        rafStr.writeBytes("Parentname:Mr. Daniels"+ "\r\n");
        rafStr.writeBytes("\r\n");
        rafStr.writeBytes("Student:Mariam"+ "\r\n");
        rafStr.writeBytes("Parentname:Ms. Thomaas"+ "\r\n");
        
        RandomAccessFile rafPrt = new RandomAccessFile("C:/ITCenter/parents.txt", "rw");
        for (int i=0;i<ln;i++){
            rafPrt.readLine();
        }
        rafPrt.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafPrt.writeBytes("Student:Assatu"+ "\r\n");
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafPrt.writeBytes("Student:Yusufu"+ "\r\n");
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafPrt.writeBytes("Student:Hashim"+ "\r\n");
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Savage"+ "\r\n");
        rafPrt.writeBytes("Student:Fatumata"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Mr. Waller"+ "\r\n");
        rafPrt.writeBytes("Student:Thomas"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Mr. Jackson"+ "\r\n");
        rafPrt.writeBytes("Student:Victor"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafPrt.writeBytes("Student:Alice"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafPrt.writeBytes("Student:Maritta"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Wilson"+ "\r\n");
        rafPrt.writeBytes("Student:Angeline"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Johnson"+ "\r\n");
        rafPrt.writeBytes("Student:Arthur"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Mr. Jackson"+ "\r\n");
        rafPrt.writeBytes("Student:Anthony"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Thomas"+ "\r\n");
        rafPrt.writeBytes("Student:Lisa"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Mr. Daniels"+ "\r\n");
        rafPrt.writeBytes("Student:Steven"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Mr. Daniels"+ "\r\n");
        rafPrt.writeBytes("Student:Stepheny"+ "\r\n");        
        rafPrt.writeBytes("\r\n");
        rafPrt.writeBytes("Parentname:Ms. Thomas"+ "\r\n"); 
        rafPrt.writeBytes("Student:Mariam"+ "\r\n");
        
        RandomAccessFile rafTr = new RandomAccessFile("C:/ITCenter/timetable.txt", "rw");
        for (int i=0;i<ln;i++){
            rafTr.readLine();
        }
        rafTr.writeBytes("Session Date , Start Time , End Time , Subject , Tutor"+ "\r\n");
        rafTr.writeBytes("15-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("15-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("15-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("15-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("18-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("18-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("18-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("18-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("19-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("19-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("19-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("19-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("20-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("20-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("20-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("20-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("21-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("21-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("21-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("21-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("22-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("22-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("22-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("22-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("25-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("25-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("25-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("25-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("26-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("26-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("26-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("26-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("27-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("27-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("27-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("27-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("28-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("28-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("28-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("28-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("29-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("29-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("29-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("29-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("30-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("30-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("30-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("30-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("01-May-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("01-May-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("01-May-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("01-May-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("04-May-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("04-May-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("04-May-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("04-May-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("05-May-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("05-May-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("05-May-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("05-May-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("06-May-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("06-May-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("06-May-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("06-May-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("07-May-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("07-May-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("07-May-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("07-May-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("08-May-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("08-May-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("08-May-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("08-May-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("11-May-20 / 09:00 / 10:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("11-May-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("11-May-20 / 11-01 / 12:00 / Computer Network / Prof Egan "+ "\r\n");
        rafTr.writeBytes("11-May-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafTr.writeBytes("12-May-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafTr.writeBytes("12-May-20 / 10:00 / 11:01 / Program Design / Prof. Cheng"+ "\r\n");
        rafTr.writeBytes("12-May-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafTr.writeBytes("12-May-20 / 12-01 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
                    
        RandomAccessFile rafLessons = new RandomAccessFile("C:/ITCenter/lessons.txt", "rw");
        for (int i=0;i<ln;i++){
            rafLessons.readLine();
        }
        rafLessons.writeBytes("Session Date , Start Time , End Time , Subject , "
                + "Tutor , Student , Cancel"+ "\r\n");
        rafLessons.writeBytes("15-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane / YUSUFU"+ "\r\n"); 
        rafLessons.writeBytes("18-Apr-20 / 12-01 / 13:00 / Computer Network / Prof Egan / YUSUFU"+ "\r\n"); 
        rafLessons.writeBytes("20-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane / YUSUFU"+ "\r\n"); 
        rafLessons.writeBytes("21-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane / YUSUFU"+ "\r\n"); 
        rafLessons.writeBytes("22-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng / YUSUFU"+ "\r\n"); 
        rafLessons.writeBytes("15-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan / ARTHUR"+ "\r\n"); 
        rafLessons.writeBytes("19-Apr-20 / 10:01 / 11:00 / Software Eng. / Prof. Lane / ARTHUR"+ "\r\n"); 
        rafLessons.writeBytes("20-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang / ARTHUR"+ "\r\n"); 
        rafLessons.writeBytes("21-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng / ARTHUR"+ "\r\n"); 
        rafLessons.writeBytes("22-Apr-20 / 09:00 / 10:00 / Data Driven Sys. / Prof. Xiang / ARTHUR"+ "\r\n"); 
        rafLessons.writeBytes("15-Apr-20 / 12:01 / 13:00 / E-Media Design / Prof. Bennett / MARIAM"+ "\r\n"); 
        rafLessons.writeBytes("18-Apr-20 / 11:01 / 12:00 / Software Eng. / Prof. Lane / MARIAM"+ "\r\n");
        rafLessons.writeBytes("19-Apr-20 / 11-01 / 12:00 / Computer Network / Prof Egan / MARIAM"+ "\r\n");
        rafLessons.writeBytes("20-Apr-20 / 10:00 / 11:01 / Program Design / Prof. Cheng / MARIAM"+ "\r\n");
        rafLessons.writeBytes("21-Apr-20 / 09:00 / 10:00 / Program Design / Prof. Cheng / MARIAM"+ "\r\n");
        
        RandomAccessFile rafMeeting = new RandomAccessFile("C:/ITCenter/meeting.txt", "rw");
        for (int i=0;i<ln;i++){
            rafMeeting.readLine();
        }
        rafMeeting.writeBytes("Meeting Date , Start Time , End Time , Subject , "
                + "Tutor"+ "\r\n");
        rafMeeting.writeBytes("15-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("15-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("15-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("15-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("18-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("18-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("18-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("18-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("19-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");     
        rafMeeting.writeBytes("19-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("19-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("19-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("20-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("20-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("20-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("20-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("21-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("21-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("21-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("21-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("22-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("22-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("22-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("22-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("25-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("25-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("25-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("25-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("26-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("26-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("26-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("26-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("27-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("27-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("27-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("27-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("28-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("28-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("28-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("28-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("29-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("29-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("29-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("29-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("30-Apr-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("30-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("30-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("30-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("01-May-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("01-May-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("01-May-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("01-May-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("04-May-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("04-May-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("04-May-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("04-May-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("05-May-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("05-May-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("05-May-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("05-May-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("06-May-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("06-May-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("06-May-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("06-May-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("07-May-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("07-May-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("07-May-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("07-May-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("08-May-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("08-May-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("08-May-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("08-May-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("11-May-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett"+ "\r\n");
        rafMeeting.writeBytes("11-May-20 / 11:00 / 11:30 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("11-May-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane "+ "\r\n");
        rafMeeting.writeBytes("11-May-20 / 12-30 / 13:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("12-May-20 / 10:00 / 11:00 / Computer Network / Prof Egan "+ "\r\n");
        rafMeeting.writeBytes("12-May-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang"+ "\r\n");
        rafMeeting.writeBytes("12-May-20 / 11:30 / 12:00 / Program Design / Prof. Cheng"+ "\r\n");
        rafMeeting.writeBytes("12-May-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane "+ "\r\n");
        
        RandomAccessFile rafSchedule = new RandomAccessFile("C:/ITCenter/schedule.txt", "rw");
        for (int i=0;i<ln;i++){
            rafLessons.readLine();
        }
        rafSchedule.writeBytes("Meeting Date , Start Time , End Time , Subject , "
                + "Tutor , Parent , Cancel"+ "\r\n");
        rafSchedule.writeBytes("15-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane / MS. THOMAS"+ "\r\n");
        rafSchedule.writeBytes("19-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett / MS. THOMAS"+ "\r\n");
        rafSchedule.writeBytes("19-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan / MS. THOMAS"+ "\r\n");
        rafSchedule.writeBytes("20-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng / MS. THOMAS"+ "\r\n");
        rafSchedule.writeBytes("21-Apr-20 / 11:30 / 12:00 / Software Eng. / Prof. Lane / MS. THOMAS"+ "\r\n");
        rafSchedule.writeBytes("18-Apr-20 / 11:30 / 12:00 / Program Design / Prof. Cheng / MS. SAVAGE"+ "\r\n");
        rafSchedule.writeBytes("19-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng / MS. SAVAGE"+ "\r\n");
        rafSchedule.writeBytes("20-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang / MS. SAVAGE"+ "\r\n");
        rafSchedule.writeBytes("21-Apr-20 / 10:00 / 11:00 / E-Media Design / Prof. Bennett / MS. SAVAGE"+ "\r\n");
        rafSchedule.writeBytes("22-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang / MS. SAVAGE"+ "\r\n");
        rafSchedule.writeBytes("15-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan / MR. WALLER"+ "\r\n");
        rafSchedule.writeBytes("18-Apr-20 / 11:00 / 11:30 / Data Driven Sys. / Prof. Xiang / MR. WALLER"+ "\r\n");
        rafSchedule.writeBytes("18-Apr-20 / 12-30 / 13:00 / Software Eng. / Prof. Lane / MR. WALLER"+ "\r\n");
        rafSchedule.writeBytes("19-Apr-20 / 11:00 / 11:30 / Program Design / Prof. Cheng / MR. WALLER"+ "\r\n");
        rafSchedule.writeBytes("19-Apr-20 / 12-30 / 13:00 / Computer Network / Prof Egan / MR. WALLER"+ "\r\n");
    }
}
