/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author yusufu.savage
 */
public class Timetable extends javax.swing.JFrame {

    /**
     * Creates new form Timetable
     */
    public Timetable() {
        initComponents();
        
        // Define variables for below if statement.
        String tableEnv = StudentLogin.linkBotton();
        int v;
        
        // Test condition using informatin for StudentLogin form class
        if (tableEnv == "Return to Student Login"){
            v = 1;
        }else{
            v = 2;
        }
        
        // Set basic information for logon menu and title base on type of user.
        switch (v){
            case 1:
                lblTitle3.setText("Logon parent");
                lblTitle1.setText("Tutors Meeting Hours");
                this.setTitle("Meetings");
                break;
                
            case 2:
                lblTitle3.setText("Logon student:");
                break;
        }
        loadTableData(); // Load data in to JTable when form start 
    }
    
    // Instantiation of use classes
    DisplaySelectionDialog dSelected = new DisplaySelectionDialog();
    StudentLogin login = new StudentLogin();
    
    // Mehtod to Load data in to JTable
    void loadTableData(){
        File file = null;
        
        // Locate file
        if(lblTitle3.getText() == "Logon parent")
        {
            String filePath = "C:\\ITCenter\\meeting.txt";
            file = new File(filePath); // Store in a variable
            txtName.setText(StudentLogin.parentName().toUpperCase()); // Set logon name
            
        }
        else if (lblTitle3.getText() == "Logon student:")
        {
            String filePath = "C:\\ITCenter\\timetable.txt";      
            file = new File(filePath); // Store in a variable
            txtName.setText(StudentLogin.studentName().toUpperCase()); // Set logon name
        }
        
        
        try {
            //Access file for data upload
            BufferedReader br = new BufferedReader(new FileReader(file));
            String firstLine = br.readLine().trim();
            //Make first line as columns heading and identify by the use of ",".
            String[] columnsName = firstLine.split(",");
            tblTimetable.setAutoCreateRowSorter(true);
            
            DefaultTableModel model = (DefaultTableModel)tblTimetable.getModel();
            model.setColumnIdentifiers(columnsName);
            
            // Create table lines
            Object[] tableLines = br.lines().toArray();
                
            // Write lines to table.
            for(int i = 0; i < tableLines.length; i++){
                String line = tableLines[i].toString().trim();
                String[] dataRow = line.split("/");
                model.addRow(dataRow);
            }
                        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Timetable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Timetable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Display session detail in pop up dialog for user to save booking
    void showSession(){
        // Store selected in variable indexs
        int indexs = tblTimetable.getSelectedRow();
        TableModel model = tblTimetable.getModel();// Create table model
        
        // Create variable to store columns values of selected row
        Object[] row = new Object[5]; 
        
        // Create columns value based on selection
        row[0] = model.getValueAt(indexs, 0).toString();
        row[1] = model.getValueAt(indexs, 1).toString();
        row[2] = model.getValueAt(indexs, 2).toString();
        row[3] = model.getValueAt(indexs, 3).toString();
        row[4] = model.getValueAt(indexs, 4).toString();
        
        // Show session detail form and table ready to receive selected data.
        dSelected.setVisible(true);
        dSelected.pack();
        dSelected.setLocationRelativeTo(null);
        dSelected.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        // Transfering selected row data to Session Detail table for display.
        dSelected.txtDate.setText((String) row[0]);
        dSelected.txtStartTime.setText((String) row[1]);
        dSelected.txtEndTime.setText((String) row[2]);
        dSelected.txtTutor.setText((String) row[3]);
        dSelected.txtSubject.setText((String) row[4]);
        
        String tableEnv = StudentLogin.linkBotton();
        if (tableEnv == "Return to Student Login"){
            dSelected.txtStudent.setText(StudentLogin.parentName());
            dSelected.btnBook.setText("Schedule");
            dSelected.lblStudent.setText("Parent        :");
            dSelected.lblTitle1.setText("Meeting Detail");
        }else{
            dSelected.txtStudent.setText(StudentLogin.studentName());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblTimetable = new javax.swing.JTable();
        lblTitle = new javax.swing.JLabel();
        lblTitle1 = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblTitle3 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        btnBooked = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Timetable");
        setName("frmTimetable"); // NOI18N
        setResizable(false);

        tblTimetable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblTimetable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTimetableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblTimetable);

        lblTitle.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle.setText("Inspire Tuition Centre (ITC)");

        lblTitle1.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle1.setText("Timetable");
        lblTitle1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnRefresh.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnRefresh.setText("Refresh Table");
        btnRefresh.setActionCommand("");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        lblTitle3.setFont(new java.awt.Font("AR JULIAN", 1, 18)); // NOI18N
        lblTitle3.setText("Logon student:");

        txtName.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        txtName.setForeground(new java.awt.Color(0, 153, 0));
        txtName.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitle3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnBooked.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBooked.setText("Display Booked Classes");
        btnBooked.setActionCommand("");
        btnBooked.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBookedActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnRefresh)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBooked)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblTitle1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(166, 166, 166))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, Short.MAX_VALUE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefresh)
                    .addComponent(btnBooked))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleName("School Timetable");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tblTimetableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTimetableMouseClicked
        // Display session detail in pop up dialog for user to save booking
        showSession();
    }//GEN-LAST:event_tblTimetableMouseClicked

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // Reload data from file to show data iniatial layout.
        DefaultTableModel model = (DefaultTableModel)tblTimetable.getModel();
        model.fireTableDataChanged();
        model.fireTableStructureChanged();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnBookedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBookedActionPerformed
        // Create and display form with Session booked per student 
        ClassSessions init = new ClassSessions();
        init.setVisible(true);
        // Carry over logon name to Sessions booked detail menu bar and load data.
        init.loadBookedTableData();
        // Close timetable form 
        this.setVisible(false);
    }//GEN-LAST:event_btnBookedActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBooked;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTitle;
    public javax.swing.JLabel lblTitle1;
    public javax.swing.JLabel lblTitle3;
    private javax.swing.JTable tblTimetable;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
