/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author yusufu.savage
 */
public class ClassSessions extends javax.swing.JFrame {

    /**
     * Creates new form ClassSessions
     */
    public ClassSessions() {
        initComponents();
        
        // Define variables for below if statement.
        String tableEnv = StudentLogin.linkBotton();
        int v;
        
        // Test condition using informatin for StudentLogin form class
        if (tableEnv == "Return to Student Login"){
            v = 1;
        }else{
            v = 2;
        }
        
        // Set basic information for logon menu and title base on type of user.
        switch (v){
            case 1:
                lblTitle3.setText("Logon parent");
                lblTitle1.setText("Schedule Meetings");
                this.setTitle("Scheduled Meetings");
                break;
                
            case 2:
                lblTitle3.setText("Logon student:");
                break;
        }
    }
    int ln; // Global variable declaration
    // Dialog class instantiation
    StudentLogin login = new StudentLogin();

    // Delete selected row from table.
    // Was not implement due to time limitation.
    protected void removeRows(final int[] rows) {
        DefaultTableModel model = (DefaultTableModel)tblSessions.getModel();
        int modelRows[] = new int[rows.length];
        for(int i = 0; i < rows.length; ++i) {
            modelRows[i] = tblSessions.convertRowIndexToModel(rows[i]);
        }
        Arrays.sort(modelRows);
        for(int i = modelRows.length - 1; i >= 0; --i) {
            int row = modelRows[i];
            model.removeRow(row);
        }
        model.fireTableDataChanged();
        
        tblSessions.setAutoCreateRowSorter(true);
    }
    
    // Initialize and store file entry in global variable 'ln'.
    void countLineSession() throws IOException{
        try {
            ln=1;
            if(btnRefereshBooking.getText() == "Cancel Class"){
                RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/lessons.txt", "rw");
                for (int i=0; raf.readLine() !=null; i++){
                    ln++;
                }
//                System.out.println("Number of Lines in Parent File: "+ln);
            }else{
                RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/meeting.txt", "rw");
                for (int i=0; raf.readLine() !=null; i++){
                    ln++;
                }
//                System.out.println("Number of Lines in Student File: "+ln);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Update deleted table
    // Was not implement due to time limitation.
    void appendCancelData(String date, String sTime, String eTime, String tutor,
            String subject, String stdName) throws IOException{
       
            try {
                RandomAccessFile rafStr = new RandomAccessFile("C:/ITCenter/lessons.txt", "rw");
                for (int i=0;i<ln;i++){
                    rafStr.readLine();
                }

                rafStr.writeBytes(date +"/");
                rafStr.writeBytes(sTime +"/");
                rafStr.writeBytes(eTime +"/");
                rafStr.writeBytes(tutor +"/");
                rafStr.writeBytes(subject +" / ");
                rafStr.writeBytes("cancelled" +" / ");
                rafStr.writeBytes(stdName +"\r\n");
                
                JOptionPane.showMessageDialog(null, "Booked for "+tutor+" on "+
                        date+ "Lesson Start at "+ sTime);
                this.setVisible(false);
                
                tblSessions.setAutoCreateRowSorter(true);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(DisplaySelectionDialog.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DisplaySelectionDialog.class.getName()).log(Level.SEVERE, null, ex);
            }

    }
    
    // Method for loading data on to JTable
    public void loadBookedTableData(){
        // Locate file
        File file = null;
        
        if(lblTitle3.getText() == "Logon parent")
        {
            String filePath = "C:\\ITCenter\\schedule.txt";      
            file = new File(filePath); // Store in a variable
            txtName.setText(StudentLogin.parentName().toUpperCase()); // Set logon name
        }
        else if (lblTitle3.getText() == "Logon student:")
        {
            String filePath = "C:\\ITCenter\\lessons.txt";      
            file = new File(filePath); // Store in a variable
            txtName.setText(StudentLogin.studentName().toUpperCase()); // Set logon name
        }

        try {
            //Access file for data upload
            BufferedReader br = new BufferedReader(new FileReader(file));
            String firstLine = br.readLine().trim();
            //Make first line as columns heading and identify by the use of ",".
            String[] columnsName = firstLine.split(",");
            tblSessions.setAutoCreateRowSorter(true);

            DefaultTableModel model = (DefaultTableModel)tblSessions.getModel();
            model.setColumnIdentifiers(columnsName);

            // Create table lines
            Object[] tableLines = br.lines().toArray();

            // Write lines to table.
            for(int i = 0; i < tableLines.length; i++){
                String line = tableLines[i].toString().trim();
                String[] dataRow = line.split("/");
                model.addRow(dataRow);
            }
            
            // Implement table filter
            String name = null; // variable defination and instantian
            if(lblTitle3.getText() == "Logon parent")
            {
                name = StudentLogin.parentName().toUpperCase();
            }
            else
            {
                name = StudentLogin.studentName().toUpperCase();
            }
            
            TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
            tblSessions.setRowSorter(tr);
            if (name == null){
                tr.setRowFilter(null);
            }else{
                tr.setRowFilter(RowFilter.regexFilter(name));
            }            
                        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Timetable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Timetable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void showSessionDialog(){
        // Store selected in variable indexs
        CancelSessionDialog dSelected = new CancelSessionDialog();
        int indexs = tblSessions.getSelectedRow();
        TableModel model = tblSessions.getModel();// Create table model
        
        // Create variable to store columns values of selected row
        Object[] row = new Object[5]; 
        
        // Create columns value based on selection
        row[0] = model.getValueAt(indexs, 0).toString();
        row[1] = model.getValueAt(indexs, 1).toString();
        row[2] = model.getValueAt(indexs, 2).toString();
        row[3] = model.getValueAt(indexs, 3).toString();
        row[4] = model.getValueAt(indexs, 4).toString();
        
        // Show session detail form and table ready to receive selected data.
        dSelected.setVisible(true);
        dSelected.pack();
        dSelected.setLocationRelativeTo(null);
        dSelected.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        // Transfering selected row data to Session Detail table for display.
        dSelected.txtDate.setText((String) row[0]);
        dSelected.txtStartTime.setText((String) row[1]);
        dSelected.txtEndTime.setText((String) row[2]);
        dSelected.txtTutor.setText((String) row[3]);
        dSelected.txtSubject.setText((String) row[4]);
        
        String tableEnv = StudentLogin.linkBotton();
        if (tableEnv == "Return to Student Login"){
            dSelected.txtStudent.setText(StudentLogin.parentName().toUpperCase());
            dSelected.btnCancelBook.setText("Cancel Meeting");
            dSelected.lblStudent.setText("Parent        :");
            dSelected.lblTitle1.setText("Cancel Booked Meeting");
        }
        else
        {
            dSelected.txtStudent.setText(StudentLogin.studentName().toUpperCase());
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        btnRefereshBooking = new javax.swing.JButton();
        lblTitle1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSessions = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lblTitle3 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        btnViewTimetable = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Booked Class Sessions");
        setName("Book Sessions"); // NOI18N
        setResizable(false);

        lblTitle.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle.setText("Inspire Tuition Centre (ITC)");

        btnRefereshBooking.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnRefereshBooking.setText("Refresh Table");
        btnRefereshBooking.setActionCommand("Referesh");
        btnRefereshBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefereshBookingActionPerformed(evt);
            }
        });

        lblTitle1.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle1.setText("Booked Classes");

        tblSessions.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblSessions.setName(""); // NOI18N
        tblSessions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSessionsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblSessions);
        tblSessions.getAccessibleContext().setAccessibleName("Session List");

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        lblTitle3.setFont(new java.awt.Font("AR JULIAN", 1, 18)); // NOI18N
        lblTitle3.setText("Logon student:");

        txtName.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        txtName.setForeground(new java.awt.Color(0, 153, 0));
        txtName.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitle3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnViewTimetable.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnViewTimetable.setText("View Timetable");
        btnViewTimetable.setActionCommand("Referesh");
        btnViewTimetable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewTimetableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 482, Short.MAX_VALUE)
                                .addComponent(btnRefereshBooking)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnViewTimetable))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(229, 229, 229)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTitle1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 224, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitle1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefereshBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnViewTimetable, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("Student Table");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefereshBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefereshBookingActionPerformed
        // Refresh table data
        DefaultTableModel model = (DefaultTableModel)tblSessions.getModel();
        model.fireTableDataChanged();
        model.fireTableStructureChanged();
    }//GEN-LAST:event_btnRefereshBookingActionPerformed

    private void tblSessionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSessionsMouseClicked
        // Display session detail in pup up dialog.
        showSessionDialog();  
    }//GEN-LAST:event_tblSessionsMouseClicked

    private void btnViewTimetableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewTimetableActionPerformed
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Timetable().setVisible(true);
            }
        });
        this.setVisible(false);
    }//GEN-LAST:event_btnViewTimetableActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefereshBooking;
    private javax.swing.JButton btnViewTimetable;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblTitle1;
    private javax.swing.JLabel lblTitle3;
    public javax.swing.JTable tblSessions;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}