/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITCenter.gui;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author yusufu.savage
 */
public class StudentLogin extends javax.swing.JFrame {
    private String username; //Variable used in name verification 
    private static String stdName = null; // Variable for logon student across app.
    private static String prtName = null; // Variable for logon parent across app.
    // Variable for testing condition in other class
    private static String linkBtn = null; 
    int ln; //Reading text file 
            
    /**
     * Creates new form StudentLogin
     */
    public StudentLogin() {
        initComponents();
        // Remove parent colunm & label when login form starts. 
        txtParent.setVisible(false);
        lblParent.setVisible(false);
    }

    // Verify student text file exist.
    void readFile(){
        if(btnLink.getText() == "Return to Student Login"){
                try {
                FileReader fr = new FileReader("C:/ITCenter/parents.txt");
            } catch (FileNotFoundException ex) {
                try {
                    FileWriter fw = new FileWriter("C:/ITCenter/parents.txt");
                } catch (IOException ex1) {
                    Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }else{
            try {
                FileReader fr = new FileReader("C:/ITCenter/students.txt");
            } catch (FileNotFoundException ex) {
                try {
                    FileWriter fw = new FileWriter("C:/ITCenter/students.txt");
                } catch (IOException ex1) {
                    Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
    }
    
    // Open file and write both parent and student detail during profile creation.
    void addData(String std, String prt) throws IOException{
       
        try {
            //Open text files for writing and saving of data.
            RandomAccessFile rafStd = new RandomAccessFile("C:/ITCenter/students.txt", "rw");
            RandomAccessFile rafPrt = new RandomAccessFile("C:/ITCenter/parents.txt", "rw");
            for (int i=0;i<ln;i++){
                rafStd.readLine();
                rafPrt.readLine();
            }
            // Write the following data to open files
            rafStd.writeBytes("\r\n");
            rafStd.writeBytes("Student:"+std+ "\r\n");
            rafStd.writeBytes("Parentname:"+prt+ "\r\n");
            
            rafPrt.writeBytes("\r\n");
            rafPrt.writeBytes("Parentname:"+prt+ "\r\n");
            rafPrt.writeBytes("Student:"+std+ "\r\n");
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex){
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    // Collect input from student and match name in text file (login verification)
    void logicStudent(String student) throws IOException{   
        RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/students.txt", "rw");
        String user = null, forStudent = null;
        try {  
            for (int i=0;i<ln;i+=3){
                forStudent = raf.readLine().substring(8).toLowerCase();
                user = student.toLowerCase();
                
                // Check user against data listed in text file.
                if(user.equals(forStudent)){
                    this.setVisible(false);
                    JOptionPane.showMessageDialog(null, "Openning Studnet Timetable");
                    new Timetable().setVisible(true);
                    JOptionPane.showMessageDialog(null, "Please Select a Sessions "
                            + "From Table to Book Class");
                    break;
                }else if(i==(ln-3)){ // When entry is not found, show message below
                    JOptionPane.showMessageDialog(null, "Student not Found, Please Check Entry");
                    break;
                }
                for(int k=1;k<=2;k++){
                    raf.readLine();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Collect input from parent and match name in text file (login verification)
    void logicParent(String parent) throws IOException{   
        RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/parents.txt", "rw");
        String user = null, forParent = null;
        try {  
            for (int i=0;i<ln;i+=3){
                forParent = raf.readLine().substring(11).toLowerCase();
                user = parent.toLowerCase();
                
                if(user.equals(forParent)){
                    this.setVisible(false);
                    JOptionPane.showMessageDialog(null, "Openning Tutor Free Hours Table");
                    new Timetable().setVisible(true);
                    JOptionPane.showMessageDialog(null, "Please Select To Schedule A Meeting");
                    break;
                }else if(i==(ln-3)){
                    JOptionPane.showMessageDialog(null, "Parent not Found, Please Check Entry");
                    break;
                }
                for(int k=1;k<=2;k++){
                    raf.readLine();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Text file data counter
    void countLine () throws IOException{
        try {
            ln=1;
            if(btnLink.getText() == "Return to Student Login"){
                RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/parents.txt", "rw");
                for (int i=0; raf.readLine() != null; i++){
                    ln++;
                }
            }else{
                RandomAccessFile raf = new RandomAccessFile("C:/ITCenter/students.txt", "rw");
                for (int i=0; raf.readLine() !=null; i++){
                    ln++;
                };
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Method to make to access logon user name in another class
    public static final String studentName(){
        return stdName;
    }
    
    // Method to access logon user name in another class
    public static final String parentName(){
        return prtName;
    }
    
    public static final String linkBotton(){
        return linkBtn;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblStudent = new javax.swing.JLabel();
        txtStudent = new javax.swing.JTextField();
        lblTitle = new javax.swing.JLabel();
        lblInfo = new javax.swing.JLabel();
        btnLogin = new javax.swing.JButton();
        btnRegister = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        lblTitle1 = new javax.swing.JLabel();
        lblTitle2 = new javax.swing.JLabel();
        lblParent = new javax.swing.JLabel();
        txtParent = new javax.swing.JTextField();
        lblLink = new javax.swing.JLabel();
        btnLink = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ITCenter Login");
        setBackground(new java.awt.Color(0, 102, 102));
        setName("frmStLogin"); // NOI18N
        setResizable(false);

        lblStudent.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblStudent.setText("Student Name  :");

        lblTitle.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle.setText("Inspire Tuition Centre (ITC)");

        lblInfo.setFont(new java.awt.Font("Bookman Old Style", 1, 18)); // NOI18N
        lblInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInfo.setText("Student: Please Login");
        lblInfo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnRegister.setText("Register");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblTitle1.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle1.setText("WELCOME");

        lblTitle2.setFont(new java.awt.Font("AR JULIAN", 0, 24)); // NOI18N
        lblTitle2.setText("TO");

        lblParent.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblParent.setText("Parent Name    :");

        txtParent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtParentActionPerformed(evt);
            }
        });

        lblLink.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLink.setText("Parent: Please click the button below to book appointment.");

        btnLink.setText("Parent Login Here!!");
        btnLink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLinkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTitle1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblTitle2))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblStudent)
                                    .addComponent(lblParent))
                                .addGap(42, 42, 42))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnRegister)
                                .addGap(82, 82, 82)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtStudent)
                            .addComponent(txtParent)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(btnLink))
                    .addComponent(lblLink, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(lblTitle1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitle2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInfo)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblStudent)
                    .addComponent(txtStudent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtParent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblParent))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRegister)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnLogin)
                        .addComponent(btnClear)))
                .addGap(18, 18, 18)
                .addComponent(lblLink)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLink)
                .addGap(25, 25, 25))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        
         // Set login evironment when user cancel registration.
        if (btnLogin.getText() == "Cancel"){
            lblInfo.setText("Student: Please Login");
            txtParent.setVisible(false);
            lblParent.setVisible(false);
            btnLogin.setText("Login");
            btnRegister.setText("Register");
            txtStudent.setText("");
            txtParent.setText("");
            // Display cancilation message.
            JOptionPane.showMessageDialog(null, "Cancel Registration");
             
            }else {
                // Prompt imput for as long as login text box is empty
                while (txtStudent.getText().isEmpty() & txtParent.getText().isEmpty()){               
                        JOptionPane.showMessageDialog(null, "Provide User Name");
                        break;
                    }
            } 
            if(!txtStudent.getText().isEmpty()){
                try {
                    stdName = (String)txtStudent.getText();
                    readFile(); // Verify login files exist
                    countLine(); // Populate data in file and store in global variable "ln".
                    logicStudent(txtStudent.getText()); // Match login user name in file                   
                } catch (IOException ex) {
                    Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (!txtParent.getText().isEmpty()){
                try {
                    prtName = (String)txtParent.getText();
                    linkBtn = (String)btnLink.getText();
                    readFile();
                    countLine();
                    logicParent(txtParent.getText());
                } catch (IOException ex) {
                    Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }//GEN-LAST:event_btnLoginActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
            
            // Set form environment for student registration
            if(btnRegister.getText()=="Register"){
                lblInfo.setText("Student: Registration");
                btnRegister.setText("Save");
                btnLogin.setText("Cancel");
                txtParent.setVisible(true);
                lblParent.setVisible(true);
            }else{ while(txtParent.getText().isEmpty()){
                // Prompt for user imput.
                JOptionPane.showMessageDialog(null, "Please Provide Info.");
                break;
            }
            if (!txtStudent.getText().isEmpty() & !txtParent.getText().isEmpty()){
                try {
                    // call necessary function to store data.
                    readFile();
                    countLine();
                    addData(txtStudent.getText(), txtParent.getText());
                    btnRegister.setText("Register");
                    txtParent.setVisible(false);
                    lblParent.setVisible(false);
                    btnLogin.setText("Login");
                    txtStudent.setText("");
                } catch (IOException ex) {
                    Logger.getLogger(StudentLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_btnRegisterActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // Clear form entry.
        txtStudent.setText("");
        txtParent.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnLinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLinkActionPerformed
        // Set parent login environment.
        if(btnLink.getText() == "Parent Login Here!!"){
            lblInfo.setText("Parent: Please Login");
            lblStudent.setVisible(false);
            txtStudent.setVisible(false);
            lblParent.setVisible(true);
            txtParent.setVisible(true);
            btnRegister.setVisible(false);
            btnLogin.setText("Login");
            btnLink.setText("Return to Student Login");
            txtStudent.setText("");
            txtParent.setText("");
        }else{
            // Otherwise set student login environment
            lblInfo.setText("Student: Please Login");
            lblStudent.setVisible(true);
            txtStudent.setVisible(true);
            lblParent.setVisible(false);
            txtParent.setVisible(false);
            btnRegister.setVisible(true);
            btnRegister.setText("Register");
            btnLink.setText("Parent Login Here!!");
            txtStudent.setText("");
            txtParent.setText("");
        }
    }//GEN-LAST:event_btnLinkActionPerformed

    private void txtParentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtParentActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtParentActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnLink;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnRegister;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblLink;
    private javax.swing.JLabel lblParent;
    private javax.swing.JLabel lblStudent;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblTitle1;
    private javax.swing.JLabel lblTitle2;
    private javax.swing.JTextField txtParent;
    private javax.swing.JTextField txtStudent;
    // End of variables declaration//GEN-END:variables
}